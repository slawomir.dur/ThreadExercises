#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>

std::mutex m;
std::condition_variable cv;
bool ready = false;

void print_num(const int& num)
{
	std::unique_lock<std::mutex> lg{m};
	while (!ready) cv.wait(lg);
	std::cout << "I'm " << num << std::endl;
}

void go()
{
	std::unique_lock<std::mutex> lg{m};
	ready = true;
	cv.notify_all();
}

int main()
{
	using std::vector;
	using std::thread;
	using std::cout;
	using std::cin;
	vector<thread> t(12);
	for (int i = 0; i < 12; i++)
		t[i] = thread(print_num, i);
	
	std::cout << "Ready? Ready...\n";
	cout << "GO: [y/n]";
	char mode;
	cin >> mode;
	while (true)
	{
		cin.get();
		if (mode == 'y')
			go();
		else if (mode == 'x')
			break;
		cin >> mode;
	}
	
	for (auto& i: t) i.join();
	
	return 0;
}