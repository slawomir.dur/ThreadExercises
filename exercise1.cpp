#include <iostream>
#include <thread>
#include <vector>

int main()
{
	using std::thread;
	using std::vector;
	
	vector<thread> vec(10);
	
	auto print_some_info = []()
	{
		std::cout << "I come from thread: " << std::this_thread::get_id() << std::endl;
	};
	
	for (auto& i : vec)
		i = thread(print_some_info);
	
	for (auto& i: vec) i.join();
	

	return 0;
}