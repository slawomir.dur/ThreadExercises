#include <thread>
#include <vector>
#include <cstdlib>

auto run = []()
{
	system("./exercise3");
};

int main()
{
	std::vector<std::thread> th(2);
	for (auto& i: th)
		i = std::thread(run);
	system("./exercise3");
	
	for (auto& i: th)
		i.join();
	return 0;
}