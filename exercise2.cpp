#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <functional>
#include <algorithm>

std::mutex m;

void function(int& x);

struct callable
{
	void operator()() {std::unique_lock<std::mutex> lck{m}; std::cout << "I come from callable!\n" << std::endl; };
};

int main()
{
	int var = 1;
	auto lambda = [&var]()->void
	{
		std::unique_lock<std::mutex> lck{m};
		std::cout << "Incrementing and printing from lambda: " << ++var << std::endl;
		std::cout << "Thread id: " << std::this_thread::get_id() << std::endl;
	};
	
	std::vector<std::function<void()>> funcs(3);
	funcs[0] = std::bind(function, var);
	funcs[1] = lambda;
	funcs[2] = callable();
	
	std::vector<std::thread> t{3};	
	for_each(t.begin(), t.end(), [=](std::thread& x){ static int num = 0; x = std::thread{funcs[num++]};});
	
	for (auto& i : t) i.join();
	
	return 0;
}

void function(int& x)
{
	std::unique_lock<std::mutex> lck{m};
	std::cout << "Incrementing and printing from function: " << x++ << std::endl;
	std::cout << "Thread id: " << std::this_thread::get_id() << std::endl;
}

		